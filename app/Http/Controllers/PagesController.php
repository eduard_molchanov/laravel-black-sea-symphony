<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Poster;
use App\Video;

class PagesController extends Controller
{
    public function multimedia()
    {
        $video = Video::all();
        $foto = Foto::all();
        return view("multimedia", compact("video", "foto"));
    }

    public function video($id)
    {
        $videos = Video::all();
        $video = Video::find($id);
        return view('video', compact("video", "videos"));
    }

    public function scanDir($dir)
    {
        $img = [];
        $scan = scandir($dir);
        for ($i = 0; $i < count($scan); $i++) {
            if ($scan[$i] != "." && $scan[$i] != "..") {
                $img [] = $scan[$i];
            }
        }
        return $img;
    }

    public function foto($id)
    {
        $fotos = Foto::all();
        $foto = Foto::find($id);
        $dir = base_path() . "/public/images/foto/" . $foto->catalog;
        $img = $this->scanDir($dir);
        return view('foto', compact("foto", "fotos", "img"));
    }

    public function about()
    {
        $dir = base_path() . "/public/images/about/u1";
        $img = $this->scanDir($dir);
        return view("about", compact("img"));
    }

    public function concerts()
    {
        $posters = Poster::all();
        return view("concerts", compact("posters"));
    }

    public function concert($id)
    {
        $poster = Poster::find($id);
        return view("concert", compact("poster"));
    }

    public function poster()
    {
        $posters = Poster::orderBy("date")->paginate(4);
        return view("poster", compact("posters"));
    }
}
