@extends("layout")
@section("title")Об оркестре @endsection
@section("content")

    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h2>Об оркестре</h2>
        <hr>

        <img src="public/images/1.jpg" alt='Об оркестре'
             style="box-shadow:15px 10px 15px rgba(0,0,0,0.5);border-radius: 10px;margin-top: 35px; max-width: 790px;"/>

        <div style="margin-top: 35px;text-align:justify;">
            <p style="text-align:justify">&nbsp;&nbsp;&nbsp; Симфонический оркестр в Ялте был организован в 1937 году
                решением Президиума Ялтинского городского Совета. С 1939 года концертная деятельность оркестра бурно
                развивалась. Программы составлялись и утверждались на особых совещаниях в Москве. Популярнейшие артисты
                того времени А. Вертинский, К.Шульженко и другие участвовали в концертах. После Великой Отечественной
                войны оркестр возобновил приостановленную работу, постепенно расширяя репертуар и укомплектовывая
                состав. С 1 января 1973 года Государственная филармония на Южном берегу Крыма была переименована в
                Крымскую Государственную филармонию, и оркестр получил нынешнее название: симфонический оркестр Крымской
                государственной филармонии. С 2001 года симфонический оркестр существует, как самостоятельное отделение
                Крымской государственной филармонии. В 2001 году на конкурсе &laquo;Общественное признание Ялты&raquo; в
                номинации &laquo;Культура&raquo; симфонический оркестр признан Коллективом года.&nbsp; В настоящее время
                коллектив возглавляет И. А. Каждан. Дирижеры &ndash; заслуженный артист Украины С. Вишняк и С. Скрипник.
                В разные годы с оркестром сотрудничали многие выдающиеся деятели искусств и творческие коллективы,
                представляющие гордость и славу отечественного и зарубежного искусства.<br/>
                Репертуар оркестра в настоящее время включает почти все классические произведения, созданные для
                большого симфонического оркестра за три столетия. В 1997 году вышел двойной альбом компакт-дисков с
                записями шедевров мировой классики и&nbsp; произведениями современных крымских композиторов. Огромный
                вклад внесен оркестром в создание фонда записей классической музыки Крымского телевидения. Значительным
                событием в жизни оркестра стало участие коллектива в грандиозных международных фестивалях классической
                музыки &laquo;Крымские зори&raquo; (с 1973 г.), &laquo;Бархатные сезоны&raquo;, &laquo;Звезды планеты&raquo;
                (1999 &ndash; 2007 годы), международных фестивалях искусства &laquo;У черного моря&raquo;, международных
                телекино форумах &laquo;Вместе&raquo;. С 1996 года оркестр неизменно принимает участие в проводимых в
                Крыму международных конкурсах пианистов им. А. Караманова. В 2014 г. АСО принял участие в фестивалях,
                впервые проводимых в Российском Крыму: &quot;Opus 25&quot;, посвященном 25-летию национальной
                всероссийской газеты &quot;Музыкальное обозрение&quot; и Арфовом фестивале в честь 50-летия Российского
                арфового общества.<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Новым словом в культурной жизни стали большие
                концертные программы фестивалей &laquo;Ялтинские сезоны&raquo;, &laquo;Рождественские встречи&raquo;,
                проводимые под патронажем Ялтинского городского Головы, фестиваль, посвященный 100-летию выдающегося
                дирижера Н. Рахлина. Оркестр регулярно принимает участие в крупнейших мероприятиях, таких как фестиваль
                искусств &laquo;Дни Пушкина в Крыму&raquo;,&nbsp; День Конституции Украины, День Независимости Украины,
                обслуживание культурной программы международных саммитов, проводимых в Большой Ялте, концерты,
                посвященные значительным календарным датам.<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Симфонический оркестр &ndash; неизменный участник всех
                международных конкурсов и фестивалей, проводимых в Крыму. В течение последних двух десятилетий оркестр
                является единственным в мире коллективом, проводящим планомерную работу по воспитанию исполнителей из
                числа наиболее талантливых юных дарований &ndash; детей Крыма, Украины. За последние годы 9 артистов
                оркестра удостоены Почетных званий&nbsp; Украины и АР Крым.&nbsp;&nbsp; В 2007 году оркестр отметил свою
                70-летнюю годовщину и признан одним из лучших творческих коллективов Украины.&nbsp; За последние два
                года репертуар оркестра пополнился рядом новых сочинений украинских и крымских композиторов, в планах на
                сезон &ndash; постоянное сотрудничество с союзом композиторов Украины и его крымским отделением, с целью
                популяризации сочинений современных композиторов.&nbsp; &nbsp; &nbsp; &nbsp;<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Оркестр
                гастролировал в Белоруссии, Молдавии, Северной Осетии, Литве, Азербайджане, Узбекистане,&nbsp; в Москве
                (неоднократно) и других крупнейших городах России и Урала, в Украине &ndash; в столице и ведущих
                областных центрах. С 1996 г по &nbsp;2000 г. совместно с оркестром, солистами и хоровыми коллективами
                Баден-Бадена и Хайдельберга был осуществлен ряд значительных творческих проектов &ndash; в Крыму и
                Германии. В том числе &ndash; постановка кантаты К. Орфа &laquo;Кармина Бурана&raquo;.&nbsp; В июле 2007
                г. симфонический оркестр Крымгосфилармонии на одном из корпоративных концертов аккомпанировал звездам
                мировой величины &mdash; Саре Брайтман и Хосе Каррерасу.<br/>
                Симфонический оркестр &ndash; лауреат Всесоюзного конкурса (1977 год), дважды удостоен высоких
                правительственных наград: Почетная грамота Президиума Верховного Совета Украины (1987 г.) и Почетная
                грамота Совета министров Украины (1999 г).<br/>
                &nbsp;</p>

            <p style="text-align:justify"><img alt="" dir="ltr" src="public/images/lastochkino_gnezdo.jpg"
                                               style="float:left; height:250px; width:195px"/>​<br/>
                АСО проводит планомерную концертную работу, включающую абонементы в Ялте, Севастополе и Симферополе для
                детской и взрослой аудитории, отдельные концерты, проводимые в различных городах Крыма, здравницах ЮБК,
                участвует в международных конкурсах и фестивалях, проходящих в Крыму. С коллективом сотрудничают солисты
                и дирижеры Украины, СНГ и дальнего зарубежья. В период декабря-января 2013 &ndash; 14 гг. АСО по
                приглашению известного агентства Concerlirica Internacional осуществил поездку в Испанию, где был дан
                ряд концертов в различных городах.<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Творческий потенциал коллектива
                весьма обширен. В репертуаре оркестра произведения различных эпох, композиторов Европы, США, стран СНГ,
                украинских и крымских авторов.&nbsp;</p>

            <p style="text-align:justify">&nbsp;</p>

            <p style="text-align:justify">&nbsp;</p>

            <p style="text-align:justify">&nbsp;</p>

        </div>
        <!--    ***********************************************************      Ф О Т О    **************************************************-->

        @include("fancybox")
        <script type="text/javascript">
            $(".fancybox").fancybox({
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 100, 0, 0.75)'
                        }
                    }
                }
            });
        </script>

        <div>
            <style>
                a img {
                    box-shadow: 15px 10px 15px rgba(0, 0, 0, 0.5);
                    border-radius: 10px;
                    margin: 25px;
                }

            </style>

            <!--    *********************************************************** место для фотографий    **************************************************-->
            @foreach($img as $i)
                <a class="fancybox" rel="group" href="public/images/about/u1_b/{{$i}}" title=''>
                    <img src="/public/images/about/u1/{{$i}}" alt=''/>
                </a>
        @endforeach

        <!--    *********************************************************** место для фотографий    **************************************************-->
        </div>
        <!--    ***********************************************************      Ф О Т О    *******************************************************************-->


@endsection
