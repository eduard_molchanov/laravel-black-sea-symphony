@extends("layout")

@section("content")

    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h2>{{$poster->name}}</h2>
        <hr>
        <span
            style="display: block; margin-top: 25px; margin-bottom: 25px; color: #6699cc; font-weight: bold;">{{date('d.m.Y', strtotime($poster->date))}}</span>
        <img src="/public/images/posters/{{$poster->img}}" width="500"/>
        <p style="text-align: justify;">
            {!! $poster->description !!}
        </p>

        <div style="height: 50px;">
            <a href="/концерты" style="float: right; margin-top: 25px;"> Каталог всех концертов ... </a>
        </div>
@endsection
