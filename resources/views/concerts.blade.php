@extends("layout")
@section("title")Каталог всех концертов @endsection
@section("content")

    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h2> Каталог всех концертов </h2>
        <hr>

        <div style="max-height: 800px; overflow: auto; margin-top: 25px;">

            @foreach ($posters as $i)

                <p>
                    <a href="/концерт/{{$i->id}}">
                <span
                    style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 12px;font-weight: bold; margin-right: 25px;">{{date('d.m.Y', strtotime($i->date))}}</span>
                        <span
                            style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;"> {{$i->name}} </span>

                    </a>
                </p>

            @endforeach
        </div>


    </div>

@endsection
