@extends("layout")
@section("title")Контактная информация @endsection
@section("content")
    <script type="text/javascript" src="public/js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="public/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="public/js/myscripts.js"></script>


    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">


        <a href="/страница_в_разработке">
            <h2 style="color: #6699cc;">
                Как к нам добраться
            </h2>
        </a>


        <h2>Контактная информация:</h2>
        <br>
        <span style="font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;">
Почтовый адрес:
</span>
        <br>
        пер. Черноморский, д. 2, офис 13-14,<br>
        г. Ялта,<br>
        Республика Крым,<br>
        Российская Федерация,<br>
        298600

        <a href="/страница_в_разработке" target="_blank">
            <h4 style="color: #6699cc;">Наш форум</h4>
        </a>

        <!--
        <a href="/оркестр/об-оркестре" target="_blank">
           <h4 style="color: #6699cc;"> Об оркестре </h4>
        </a>


            <a href="/оркестр/состав" target="_blank">
               <h4 style="color: #6699cc;"> Состав оркестра </h4>
            </a>
         -->

        <div>
            <h4 style="display: inline-block; margin-right: 15px;">Мы в социальных сетях: </h4>
            <a href="http://vk.com/id221757116" target="_blank" style=" margin-right: 15px;"><img src="public/images/b.png"
                                                                                                  alt=""
                                                                                                  width="35"/></a>
            <a href="http://www.facebook.com/crimean.symphony.orchestra" target="_blank"
               style=" margin-right: 15px;"><img src="public/images/f.png" alt="Facebook" width="35"/></a>
            <a href="" target="_blank"><img src="public/images/g.png" alt="" width="38"/></a>

        </div>
        <br>
        <br>
        <img src="public/images/tz.png" width="50" style="display:block;"/>
        <p>
            Художественный руководитель и главный дирижёр<br>

            <span style="font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;">

+7 978 897 71 35  -
<a href="/И.А.Каждан" target="_blank" alt="Игорь Александрович Каждан">Игорь Александрович Каждан</a>

 </span>


        </p>

        <img src="public/images/ts.png" width="50" style="display:block;"/>
        <p>
            Главный администратор<br>
            <span style="font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;">
 +7 978 897 71 45  -

Евгения Владимировна Гусева
</span>
            <br>
        </p>
        <br>
        <p>
            <span>Е-mаіl: </span>
            <span style="font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;">academic.symphony.crimea@gmail.com
</span>

        </p>
{{--        <p style="margin-top: 35px;">--}}
{{--            или по форме обратной связи:--}}
{{--        </p>--}}


{{--        <form action="" method="post" id="email_site">--}}

{{--            <label for="first_last_name">Ваше имя и фамилия:--}}
{{--                <input type="text" class="textfield" name="name" size="40%" value=""/>--}}
{{--            </label>--}}
{{--            <br/>--}}

{{--            <label for="email">Ваш адрес e-mаil:--}}
{{--                <input type="text" class="textfield" name="email" size="40%" value=""/>--}}
{{--            </label>--}}
{{--            <br/>--}}

{{--            <textarea name="text"></textarea>--}}

{{--            <br/>--}}
{{--            <input type="submit" class="button" name="email_form" value="отправить"/>--}}

{{--        </form>--}}

        <style type="text/css">
            #content {
                background: #fcf4e8 url("public/images/bg_partitur.jpg") repeat-y right top;
            }

            #content p img#poxta {
                float: none;
                padding: 0;
                margin-bottom: -4px;
            }
        </style>
<br>
<br>
<br>
<br>
<br>
@endsection
