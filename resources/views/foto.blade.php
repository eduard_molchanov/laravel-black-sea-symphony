@extends("layout")
@section("title") {{$foto->name}} @endsection
@section("content")
    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h2> Наши фотоальбомы</h2>
        <hr>

        <div style="max-height: 150px; overflow: auto; ">
            @foreach ($fotos as $i)

                <p>
                    <a href="/фотоальбом/{{$i->id}}">
                <span
                    style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 12px;font-weight: bold; margin-right: 25px;">{{date('d.m.Y', strtotime($i->date))}}</span>
                        <span
                            style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;"> {{$i->name}} </span>

                    </a>
                </p>

            @endforeach

        </div>


        <hr style="margin-bottom: 50px;">

        <span style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 14px;font-weight: bold; margin-right: 25px;">{{date('d.m.Y', strtotime($foto->date))}}</span>
        <span style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 19px;font-weight: bold;">{{$foto->name}}</span>

        <div style="border-radius: 10px; background-color: #FDFCBC; padding: 15px; margin-top: 25px;box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px;">
            {!! $foto->description !!}
        </div>

        <!--    ***********************************************************      Ф О Т О    **************************************************-->

        @include("fancybox")

        <script type="text/javascript">
            $(".fancybox").fancybox({
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 100, 0, 0.75)'
                        }
                    }
                }
            });
        </script>

        <div>
            <style>
                a img {
                    box-shadow: 15px 10px 15px rgba(0, 0, 0, 0.5);
                    border-radius: 10px;
                    margin: 25px;
                }

            </style>

            <!--    *********************************************************** место для фотографий    **************************************************-->
            @foreach($img as $i)
                <a class="fancybox" rel="group" href="/public/images/foto/{{$foto->catalog}}_b/{{$i}}" title=''>
                    <img src="/public/images/foto/{{$foto->catalog}}/{{$i}}" alt=''/>
                </a>
        @endforeach

        <!--    *********************************************************** место для фотографий    **************************************************-->
        </div>
        <!--    ***********************************************************      Ф О Т О    *******************************************************************-->


    </div>

@endsection
