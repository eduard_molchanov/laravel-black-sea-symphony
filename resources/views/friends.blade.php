@extends("layout")
@section("title")Наши друзья и партнёры @endsection
@section("content")
    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">
        <style>
            h2 {
                color: #6699cc;
            }
        </style>
        <h2>Наши друзья и партнёры</h2>
        <hr>

        <a href="http://www.0654.com.ua/" target="_blank" title="">
            <h2>
                0654
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/0654_logo.jpg" width="300"/>
        </a>


        <a href="http://md-eksperiment.org/" target="_blank" title="">
            <h2>
                Эксперимент:
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/BANER_728x90.gif" width="550"/>
        </a>


        <a href="http://cit.ua/" target="_blank" title="">
            <h2>
                Сit.ua
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/FlBjA_GKrv0.jpg" width="300"/>
        </a>


        <a href="http://l88.gallery.ru/" target="_blank" title="">
            <h2>
                Марина Лагарто
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/marina1.jpg" width="230"/>
        </a>


        <a href="http://www.figuro-theater.de/" target="_blank" title="">
            <h2>
                Puppentheater FIGURO
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/figuro-logo-weiss.jpg" width="230"/>
        </a>


        <a href="http://www.partenit-kpo.org/" target="_blank" title="">
            <h2>
                ВОЗРОЖДЕНИЕ
            </h2>
            <img
                style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5); margin-top: 25px; margin-bottom: 25px;"
                src="public/images/friends/ppartenit-kpo.org.jpg" width="230"/>
        </a>


        <a href="http://g-kareva.ru/" target="_blank">
            <h2>
                Советская солистка Галина Алексеевна Карева
            </h2>
        </a>

        <a href="http://operaclassic.net/" target="_blank">
            <h2>
                Оперное искусство<br>Биографии русских композиторов. Сведения об оперных произведениях: композитор,
                действующие лица, сюжет, фотофрагменты. Подборка статей о театрах.
            </h2>


        </a>

@endsection
