@extends("layout")

@section("title")Главная @endsection

@section("content")
    <script type="text/javascript" src="public/js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="public/js/jquery.cross-slide.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#test1').crossSlide({
                speed: 45,
                fade: 1
            }, [
                {src: 'public/images/1.jpg', dir: 'up'},
                {src: 'public/images/2.jpg', dir: 'down'},
                {src: 'public/images/3.jpg', dir: 'up'},
                {src: 'public/images/4.jpg', dir: 'down'}
            ]);

        });
    </script>

    <style type="text/css">
        #test1 {
            margin: 1em auto;
            border: 2px solid #555;
            width: 750px;
            height: 360px;
        }

    </style>

    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto; ">

        <div id="test1" style="border-radius: 15px; box-shadow: 25px 25px 15px rgba(0,0,0,0.5);">Загружаю…</div>


        <div style="margin-top: 80px; padding:15px; height: auto; ">
            <img src="public/images/g_dir.png" width="150"
                 style="float: right; margin: 10px 35px; border-radius: 10px;box-shadow: 10px 10px 25px rgba(0,0,0,0.5);"/
            >
            <span style="
 display: block;;
 margin-top: 10px;
 margin-bottom: 25px;

 /*text-shadow:#6699ff 2px 1px 1px;
 color: #A52A2A;*/

 font-size: 28px;
 color: #6699cc;
 font-weight: bold;
 font-style: italic;
 font-family: " Lucida Sans Unicode";">Уважаемые дамы и господа! {{ base_path() }}</span>

            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Независимо от того, обдуманно Вы зашли на наш сайт или случайно, мы рады приветствовать Вас!
            </p>
            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Нынешний век настолько стремителен и перегружен информацией, что человеку просто необходимо иметь
                возможность периодически отрешаться от суеты и рутины, чтобы заглянуть вглубь себя, в свою душу,
                приобщиться к чему-то светлому и прекрасному.
            </p>
            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Одним из самых действенных и мощных средств для этого является музыка. Она пробуждает в человеке
                стремление к возвышенному, раскрывает новые грани его личности, излечивает от уныния и душевного мятежа.
                Музыка очищает, утешает, спасает.
            </p>
            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Наш оркестр – это всего лишь один из великого множества кораблей, плавающих по безбрежному океану
                симфонии. Но коллектив его – это проверенный временем союз преданных приверженцев и служителей Музыки.
                Мы стараемся увидеть привычные вещи по-новому, найти неожиданные стороны в традиционных произведениях и
                донести наше восприятие до слушателей.
            </p>
            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Если Вы устали от однообразия серых будней, мы с удовольствием поможем Вам приобщиться к сияющему миру
                гармонии и красоты. Приходите на наши концерты – и Вы узнаете много нового о мире, о музыке и о себе.
                Расписание выступлений можно найти на страничке «Афиша».
            </p>

            <!--
            <p style="text-align:justify;  text-indent: 1.5em;">
            Для тех, кто хочет попробовать себя в качестве дирижёра, мы предлагаем профессиональные мастер-классы. Если Вы ощутили себя композитором или исполнителем и хотите осуществить студийную запись ваших произведений – наш оркестр  всегда к вашим услугам. Кроме того, мы можем помочь грамотно набрать и сверстать оркестровые партии и партитуры.
            </p>

            <p style="text-align:justify;  text-indent: 1.5em;">
            Мы так же будем безмерно благодарны всем, кто  окажет благотворительную и спонсорскую помощь в развитии коллектива и осуществлении наших проектов.
            </p>
            -->
            <p style="text-align:justify;  text-indent: 1.5em;font-style: italic;">
                Искренне Ваши, адепты Музыки – Симфонический Оркестр Крымской Филармонии.

            </p>

            <h4 style="text-align:right; margin-top: 35px; margin-right: 25px;font-style: italic;"> Игорь Каждан</h4>
            <h5 style="text-align:right;margin-top: -20px; margin-right: 25px;font-style: italic;"> Главный дирижёр
                оркестра </h5>
        </div>


@endsection
