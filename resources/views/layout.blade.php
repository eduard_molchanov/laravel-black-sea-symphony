<!DOCTYPE html>
<html lang="ru">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="ru"/>

    <meta name="robots" content="all"/>
    <meta name="author" content="Академический Симфонический оркестр Крымской филармонии"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title> @yield("title") </title>

    <link href="{{ asset('public/css/format.css') }}" type="text/css" rel="stylesheet" media="all"/>


</head>
<body>


<!--    <img src="/images/a.png" style="position: fixed; width: 250px;">-->
<!--    -->
<!--    --><?php //include "newyear.php" ?>


<h1>Симфонический оркестр Крымской филармонии </h1>

@yield("content")

</div>

@include("menu")

</body>
</html>
