<ul id="navi">
    @foreach($menu as $item)
        <li><a href="{{asset($item->url)}}" class="{{ Request::is("$item->url") ? 'menu-active' : '' }}">{{$item->name}}</a></li>
        {{--        <li><a href="{{$item->url}}" class="{{ Request::is("$item->url") ? 'menu-active' : '' }}">{{$item->name}}</a></li>--}}
    @endforeach
</ul>
