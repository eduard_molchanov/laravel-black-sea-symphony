@extends("layout")
@section("title")Каталоги видео и фотоальбомов @endsection
@section("content")

    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h2> Мультимедиа </h2>
        <hr>

        <h3 style="border-radius: 10px; background-color: #FDFCBC; padding: 15px; margin-top: 25px;box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px;margin-bottom: 15px;">
            Каталог видео</h3>

        <div style="overflow: auto; max-height: 500px;">

            @foreach ($video as $i)

                <p>
                    <a href="/видео/{{$i->id}}">
                <span
                    style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 12px;font-weight: bold; margin-right: 25px;">{{date('d.m.Y', strtotime($i->date))}}</span>
                        <span
                            style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;"> {{$i->name}} </span>

                    </a>
                </p>

            @endforeach
        </div>


        <h2 style="border-radius: 10px; background-color: #FDFCBC; padding: 15px; margin-top: 50px;box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; margin-bottom: 15px;">
            Наши фотоальбомы</h2>

        <div style="max-height: 500px; overflow: auto; ">

            @foreach ($foto as $i)

                <p>
                    <a href="/фотоальбом/{{$i->id}}">
                <span
                    style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 12px;font-weight: bold; margin-right: 25px;">{{date('d.m.Y', strtotime($i->date))}}</span>
                        <span
                            style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;"> {{$i->name}} </span>

                    </a>
                </p>

            @endforeach

        </div>

@endsection
