@extends("layout")
@section("title")Афиши всех концертов @endsection
@section("content")

    <div id="content"
         style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; max-height: 1500px; overflow: auto;">

        <h2>Афиша </h2><a href="/концерты" style="float: right; margin-top: -15px;"> Каталог всех концертов ... </a>
        <hr>
        <h4> {{$posters->links('vendor.pagination.semantic-ui')}}</h4>


@include("fancybox")


        <script type="text/javascript">
            $(".fancybox").fancybox({
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 255, 0.55)'
                        }
                    }
                }
            });
        </script>

        <!--    ***********************************************************     А Ф И Ш Ы    **************************************************-->
        <div>
            <style>
                a img {
                    box-shadow: 15px 10px 15px rgba(0, 0, 0, 0.5);
                    border-radius: 10px;
                    margin: 25px;
                }

            </style>

            <!--    *********************************************************** место для афиш    **************************************************-->




            @foreach($posters as $i)
                <div style="float: left;  ">
                    <a class="fancybox" rel="group" href="/public/images/posters/{{$i->img}}"
                       title='<a href="/концерт/{{$i->id}}">читать далее ... </a> '>
                        <img src="/public/images/posters/preview/{{$i->img}}" alt='' height="450"/><br>
                    </a>

                </div>
        @endforeach


        <!--    *********************************************************** место для афиш    **************************************************-->






        </div>

        <!--    ***********************************************************      А Ф И Ш Ы    *******************************************************************-->

@endsection
