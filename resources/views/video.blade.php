@extends("layout")

@section("title"){{$video->name}} @endsection

@section("content")
    <div id="content" style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; height: auto;">

        <h4 style="text-align: right;">{{$video->date}}</h4>
        <h3>   {{$video->name}}.</h3><br>
        <div style="box-shadow:15px 10px 15px rgba(0,0,0,0.5); border-radius: 10px; width: 810px; height:485px; ">
            {!! $video->url !!}
        </div>
        <h3> {!! $video->description !!}</h3>

        <hr>
        <h3 style="margin-bottom: 35px;">Каталог видео</h3>

        <div style="overflow: auto; max-height: 500px;">

            @foreach ($videos as $i)
                <p>
                    <a href="/видео/{{$i->id}}">
                    <span
                        style="color: #6699cc;  font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 12px;font-weight: bold; margin-right: 25px;">{{$i->date}}</span>
                        <span
                            style="color: #09208f; font-family: Arial, Lucida Sans Unicode, Sans-Serif; font-size: 15px;font-weight: bold;">{{$i->name}} </span>
                    </a>
                </p>
            @endforeach
        </div>
    </div>
@endsection
