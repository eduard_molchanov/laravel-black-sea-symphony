<?php

Route::get('/', function () {
    return view('home');
});


Route::get('/афиша', "PagesController@poster")->name('афиша');

Route::get('/концерты', "PagesController@concerts");

Route::get('/концерт/{id}', "PagesController@concert");

Route::get('/о_нас', "PagesController@about");

Route::get('/мультимедия', "PagesController@multimedia");

Route::get('/видео/{id}', "PagesController@video");

Route::get('/фотоальбом/{id}', "PagesController@foto");


Route::get('/И.А.Каждан', function () {
    return view('leader');
});

Route::get('/друзья', function () {
    return view('friends');
});

Route::get('/сотрудничество', function () {
    return view('cooperation');
});

Route::get('/контакты', function () {
    return view('contacts');
});
Route::get('/страница_в_разработке', function () {
    return view('page-under-construction');
});
